Copyright 2015 Eric Duhamel

This file is part of Labyrinth.


HOW TO RUN

Labyrinth runs on the Pygame platform. For many computer systems, you must
install Pygame. Refer to the following web address for instructions.

    http://www.pygame.org/wiki/GettingStarted

Note: to install Pygame on a Debian, Ubuntu, Linux Mint, Trisquel, or similar
computer, you may simply need to install the package "python-pygame" via your
package manager or by running the command below in a terminal. Other
distributions of GNU/Linux may have a different method for installing the
package. Refer to your system's documentation if needed.

    sudo apt-get install python-pygame

Once Pygame is installed, go to the folder containing Labyrinth and open
"Labyrinth.py". You may, alternatively, open a terminal in that folder and
run the following command.

    ./Labyrinth.py

If you would like to run the game fullscreen, open the folder containing
Labyrinth and double-click "Labyrinth-fullscreen.py". Alternatively, you may
use the following command-line option.

    ./Labyinth.py --fullscreen


HOW TO PLAY

Labyrinth can be played with a keyboard or a game controller. Navigate the main
menu with your joystick and first button, or arrow keys and enter. Further
instructions are displayed on-screen.


DESKTOP FILE

On GNU/Linux systems, in order for this game to show up in your applications
menu under "Games", you'll need to create and install a .desktop file. Use the
following example.

[Desktop Entry]
Type=Application
Name=Game Name
Icon=/home/myusername/gamefolder/icon.png
Path=/home/myusername/gamefolder
Exec=/home/myusername/gamefolder/labyrinth.sh
Categories=Game

Just open up a text editor, copy, and edit the appropriate sections. You'll
need to change 'myusername' to your actual user name, change '/gamefolder' to
the location you unzipped the folder, and "Game Name". You may save the file as
"labyrinth.desktop".

Save the file and copy it to ~/.local/share/applications
'.local' is a hidden folder in your home folder. Copying this file there will
make the game appear in your applications menu under "Games".

http://www.noxbanners.net/labyrinth
