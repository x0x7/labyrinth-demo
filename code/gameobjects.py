# Copyright 2015 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import pygame, random

#LEFT = (-1, 0)
#UP = (0, -1)
#DOWN = (0, 1)
#RIGHT = (1, 0)

LEFT = 'left'
UP = 'up'
DOWN = 'down'
RIGHT = 'right'

NORTH = 1 # 0001
EAST = 2 # 0010
SOUTH = 4 # 0100
WEST = 8 # 1000

OO = 0 # 0000
NN = 1 # 0001
EE = 2 # 0010
NE = 3 # 0011
SS = 4 # 0100
SN = 5 # 0101
NS = 5 # 0101
SE = 6 # 0110
WW = 8 # 1000
NW = 9 # 1001
WE = 10 # 1010
SW = 12 # 1100

# New way of keeping track of walls
WALLSDICT = {NORTH: (NN, NE, SN, NW),
             EAST: (EE, NE, SE, WE),
             SOUTH: (SS, SN, SE, SW),
             WEST: (WW, NW, WE, SW)}

class GameObject(pygame.Rect):
    def __init__(self, name, x, y, width, height):
        self.name = name
        self.step = 0
        self.direction = 0
        super(GameObject, self).__init__((x, y, width, height))

    def moveRight(self, speed):
        self.direction = 3
        self.x += speed

    def moveLeft(self, speed):
        self.direction = 1
        self.x -= speed

    def moveDown(self, speed):
        self.direction = 2
        self.y += speed

    def moveUp(self, speed):
        self.direction = 0
        self.y -= speed

    def moveDirection(self, direction, speed):
        if direction == RIGHT:
            self.direction = 3
            self.x += speed
        elif direction == LEFT:
            self.direction = 1
            self.x -= speed
        elif direction == DOWN:
            self.direction = 2
            self.y += speed
        elif direction == UP:
            self.direction = 0
            self.y -= speed        

    def advanceSteps(self, maxsteps):
        self.step += 1
        if self.step > 8:
            self.step = 0

    def getDict(self):
        Dict = {'name': self.name + '-' + self.status,
                'x': self.x,
                'y': self.y,
                'step': self.step,
                'direction': self.direction}
        return Dict

    def getRect(self):
        return pygame.Rect(self.x, self.y, self.width, self.height)


class WallMazeSprite(GameObject):
    def __init__(self, name, dimensions):
        self.visible = True
        self.darkness = 0
        self.dead = False
        self.step = 0
        self.DEATHDELAY = 10
        self.deathAnimation = self.DEATHDELAY
        super(WallMazeSprite, self).__init__(name, dimensions[0], dimensions[1], dimensions[2], dimensions[3])

    def move(self, direction, distance):
        if direction == UP:
            self.y = self.y - distance
        elif direction == LEFT:
            self.x = self.x - distance
        elif direction == DOWN:
            self.y = self.y + distance
        elif direction == RIGHT:
            self.x = self.x + distance

    def followPath(self, wallmaze, maxDistance, currentDirection, intentDirection, wallsdict):
        distanceToPath = wallmaze.getDistanceToPath(intentDirection, self)
        pathIsOpen = wallmaze.getPathIsOpen(intentDirection, self, wallsdict)
        # To travel the center of the path, we find the
        # distance to the center of the path in the indicated
        # direction, and move toward there. Walls block movement.
        if distanceToPath == 0 and pathIsOpen:
            return intentDirection, maxDistance
        elif distanceToPath <= maxDistance:
            return currentDirection, distanceToPath
        else:
            return currentDirection, maxDistance


class Missile(WallMazeSprite):
    fired = False

    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height


class Shooter(WallMazeSprite):
    counter = 0
    owner = 0
    fired = False

    def __init__(self, name, dimensions):
        super(Shooter, self).__init__(name, dimensions)
        self.x = dimensions[0]
        self.y = dimensions[1]
        self.width = dimensions[2]
        self.height = dimensions[3]
        self.dx = 1
        self.dy = 0
        self.missile = Missile(0, 0, 1, 1)

    def fireMissile(self, width, height):
        self.missile.width = width
        self.missile.height = height
        self.missile.centerx = self.centerx
        self.missile.centery = self.centery
        self.missile.fired = True

    def moveMissile(self, direction, distance):
        if direction == UP:
            self.missile.y = self.missile.y - distance
        elif direction == LEFT:
            self.missile.x = self.missile.x - distance
        elif direction == DOWN:
            self.missile.y = self.missile.y + distance
        elif direction == RIGHT:
            self.missile.x = self.missile.x + distance

    def stopMissile(self):
        self.missile.fired = False

    def targetInRange(self, rect, target):
        if (self.direction == LEFT and self.centery > target.top and self.centery < target.bottom and target.x < self.x or
                self.direction == UP and self.centerx > target.left and self.centerx < target.right and target.y < self.y or
                self.direction == DOWN and self.centerx > target.left and self.centerx < target.right and target.y > self.y or
                self.direction == RIGHT and self.centery > target.top and self.centery < target.bottom and target.x > self.x):
            return True
        else:
            return False


class WallMazeCell(pygame.Rect):
    def __init__(self, x, y, width, height, walls):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.walls = walls

    def getWall(self, direction, wallsdict):
        if direction == LEFT:
            direction = WEST
        if direction == UP:
            direction = NORTH
        if direction == DOWN:
            direction = SOUTH
        if direction == RIGHT:
            direction = EAST
#        if self.walls > 15:
#            self.cleanWalls = self.walls - 15
#        else:
#            self.cleanWalls = self.walls
#        if self.walls & direction != 0:
#            return True
#        else:
#            return False
        if self.walls in wallsdict[direction]:
            return True
        else:
            return False

    def getWalls(self):
        return self.walls


class WallMaze(pygame.Rect):
    def __init__(self, dimensions, wallmap):
        self.x = dimensions[0]
        self.y = dimensions[1]
        self.width = dimensions[2]
        self.height = dimensions[3]
        self.columns = len(wallmap[0])
        self.rows = len(wallmap)
        self.CELLWIDTH = self.width / self.columns
        self.CELLHEIGHT = self.height / self.rows
        self.wallmap = wallmap

    def set_wallmap(self, wallmap):
        self.columns = len(wallmap[0])
        self.rows = len(wallmap)
        self.CELLWIDTH = self.width / self.columns
        self.CELLHEIGHT = self.height / self.rows
        self.wallmap = wallmap

    def getCellRect(self, x, y):
        cellwidth = self.CELLWIDTH
        cellheight = self.CELLHEIGHT
        cellx = (x * cellwidth) + self.x
        celly = (y * cellheight) + self.y
        return pygame.Rect(cellx, celly, cellwidth, cellheight)

    def get_cellRect(self, x, y):
        cellwidth = self.CELLWIDTH
        cellheight = self.CELLHEIGHT
        xcell = (x - self.x) / cellwidth
        ycell = (y - self.y) / cellheight
        cellx = (xcell * cellwidth) + self.x
        celly = (ycell * cellheight) + self.y
        return pygame.Rect(cellx, celly, cellwidth, cellheight)

    def getBoxAtPixel(self, x, y):
        cellwidth = self.CELLWIDTH
        cellheight = self.CELLHEIGHT
        xcell = (x - self.x) / cellwidth
        ycell = (y - self.y) / cellheight
        return xcell, ycell

    def getLeftTopOfCell(self, x, y):
        cellwidth = self.CELLWIDTH
        cellheight = self.CELLHEIGHT
        cell_x = self.x + (cellwidth * x)
        cell_y = self.y + (cellheight * y)
        return (cell_x, cell_y)

    def getCenterOfBox(self, x, y):
        cellwidth = self.CELLWIDTH
        cellheight = self.CELLHEIGHT
        cell_x = self.x + ((cellwidth * x) + (cellwidth / 2))
        cell_y = self.y + ((cellheight * y) + (cellwidth / 2))
        return (cell_x, cell_y)

    def getCellWalls(self, x, y):
        cellwidth = self.CELLWIDTH
        cellheight = self.CELLHEIGHT
        xcell = (x - self.x) / cellwidth
        ycell = (y - self.y) / cellheight
        cell_x = (xcell * cellwidth) + self.x
        cell_y = (ycell * cellheight) + self.y
        if xcell < len(self.wallmap[0]) and xcell > -1:
            if ycell < len(self.wallmap) and ycell > -1:
                return WallMazeCell(cell_x, cell_y, cellwidth, cellheight, self.wallmap[ycell][xcell])
        return WallMazeCell(cell_x, cell_y, cellwidth, cellheight, 0)

    def getDistanceToPath(self, pathDirection, objRect):
        cellRect = self.get_cellRect(objRect.centerx, objRect.centery)
        moveRect = pygame.Rect(objRect.x, objRect.y, cellRect.width, cellRect.height)
        moveRect.centerx = objRect.centerx
        moveRect.centery = objRect.centery
        distanceToPath = 0
        if pathDirection == UP:
            if moveRect.left >= cellRect.left and moveRect.right <= cellRect.right:
                distanceToPath = 0
            else:
                distanceToPath = abs(moveRect.left - cellRect.left)
        if pathDirection == LEFT:
            if moveRect.top >= cellRect.top and moveRect.bottom <= cellRect.bottom:
                distanceToPath = 0
            else:
                distanceToPath = abs(moveRect.top - cellRect.top)
        if pathDirection == DOWN:
            if moveRect.left >= cellRect.left and moveRect.right <= cellRect.right:
                distanceToPath = 0
            else:
                distanceToPath = abs(moveRect.left - cellRect.left)
        if pathDirection == RIGHT:
            if moveRect.top >= cellRect.top and moveRect.bottom <= cellRect.bottom:
                distanceToPath = 0
            else:
                distanceToPath = abs(moveRect.top - cellRect.top)
        return distanceToPath

    def getPathIsOpen(self, pathDirection, objRect, wallsdict):
        cellRect = self.get_cellRect(objRect.centerx, objRect.centery)
        cellWalls = self.getCellWalls(objRect.centerx, objRect.centery)
        moveRect = pygame.Rect(objRect.x, objRect.y, cellRect.width, cellRect.height)
        moveRect.centerx = objRect.centerx
        moveRect.centery = objRect.centery
        pathIsOpen = True
        if pathDirection == UP and cellWalls.getWalls() in wallsdict[NORTH]:
            if moveRect.top <= cellRect.top:
                pathIsOpen = False
        if pathDirection == LEFT and cellWalls.getWalls() in wallsdict[WEST]:
            if moveRect.left <= cellRect.left:
                pathIsOpen = False
        if pathDirection == DOWN and cellWalls.getWalls() in wallsdict[SOUTH]:
            if moveRect.bottom >= cellRect.bottom:
                pathIsOpen = False
        if pathDirection == RIGHT and cellWalls.getWalls() in wallsdict[EAST]:
            if moveRect.right >= cellRect.right:
                pathIsOpen = False
        return pathIsOpen


class Point():
    def __init__(self, point):
        self.x = point[0]
        self.y = point[1]
