# Copyright 2015 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import pygame
from pygame.locals import *

class MenuItem():
    def __init__(self, title):
        self.title = title
        self.disabled = False
        self.submenu = None


class Menu():
    def __init__(self, title, items):
        self.title = title
        self.menuItems = []
        for item in items:
            self.menuItems.append(MenuItem(item))
        self.selector = 0
        self.confirmed = False

    def next(self):
        self.selector += 1
        if self.selector > len(self.menuItems) - 1:
            self.selector = 0

    def previous(self):
        self.selector -= 1
        if self.selector < 0:
            self.selector = len(self.menuItems) - 1

    def confirm(self):
        self.confirmed = True

    def cancel(self):
        self.confirmed = False

    def disableItem(self, index):
        if index > 0 and index <= len(self.menuItems):
            self.menuItems[index-1].disabled = True

    def itemTitle(self, index):
        if index >= 0 and index < len(self.menuItems):
            return self.menuItems[index].title

    def addSubMenu(self, index, items):
        if index > 0 and index <= len(self.menuItems):
            self.menuItems[index-1].submenu = Menu(self.itemTitle(index-1), items)
            return self.menuItems[index-1].submenu

    def getSubMenu(self, index):
        return self.menuItems[index].submenu

